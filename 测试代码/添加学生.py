'''
在管里平台添加学生
'''
import random

import requests

import faker
from xpinyin import Pinyin

host = "http://101.43.141.241:8000/"
f = faker.Faker(locale="ZH_CN")
p = Pinyin()

s = requests.session()
url = host + "api/user/login"
cs = {"userName": "admin", "password": "123456", "remember": False}
r = s.post(url, json=cs)

for i in range(50):
    a = f.name()
    b = p.get_pinyin(a).split("-")
    b = "".join(b)
    print(a, b)

    url = host + "api/admin/user/edit"
    cs = {"id": None, "userName": b, "password": "123456", "realName": a, "role": 1, "status": 1, "age": "",
          "sex": random.randint(1, 2), "birthDay": "2011-08-01", "phone": None, "userLevel": random.randint(1, 6)}
    r = s.post(url, json=cs)
    print(r.text)
