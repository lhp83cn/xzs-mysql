import requests

host = "http://101.43.141.241:8000/"

s = requests.session()
url = host + "api/user/login"
cs = {"userName": "admin", "password": "123456", "remember": False}
r = s.post(url, json=cs)

for i in range(3, 1000):  # 1为管理员，2为学生，不能删除
    url = f"http://101.43.141.241:8000/api/admin/user/delete/{i}"
    response = s.post(url)

    print(response.text)
